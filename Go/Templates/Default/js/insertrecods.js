$(document).ready(function() {
	//##### send add record Ajax request to response.php #########
	$("#insertrecord").click(function (e) {
			e.preventDefault();
			sms = [];
			if($("#contentText").val()==='')
			{
				sms['messsage'] = "Inserisci qualcosa";
				sms['error'] = 1;

				textonError();
				return false;
			}
			  	$('#show_error_text').show('slow');

				$("#insertrecord").hide(); //hide submit button
				$("#LoadingImage").show(); //show loading image

		 	var myData = 'content_txt='+ $("#contentText").val(); //build a post data structure
			jQuery.ajax({
			type: "POST", // HTTP method POST or GET
			url: "main.php?act=insert", //Where to make Ajax calls
			dataType:"text", // Data type, HTML, json etc.
			data:myData, //Form variables
			success:function(response){
				$("#responds").append(response);
				$("#contentText").val(''); //empty text field on successful
				$("#FormSubmit").show(); //show submit button
				$("#LoadingImage").hide(); //hide loading image
			},
			error:function (xhr, ajaxOptions, thrownError){
				$("#FormSubmit").show(); //show submit button
				$("#LoadingImage").hide(); //hide loading image
				alert(thrownError);
			}
			});
	});
// error
	function textonError(){
		if(sms.error > 0){
		$('#show_error_text').html(sms.messsage);
		$('#show_error_text').show('slow');

		$('#show_error_text').css({"backgroundColor":'#ff1a1a',
															"color":"#ffffff"
																});
		}
	}
	$("#singup").click(function (e) {
			e.preventDefault();
			sms = [];
			if($("#name").val()==='' && $("#password").val() ==='')
			{
				sms['messsage'] = "I campi sono vuoti";
				sms['error'] = 1;

				textonError();
				return false;
			}
			  	$('#show_error_text').show('slow');

				$("#singup").hide(); //hide submit button
				$("#LoadingImage").show(); //show loading image

		 	var name = $("#name").val();

			var password =  $("#password").val(); //build a post data structure
			var input = {
				 'name':name,
			 'password':password

			};
			jQuery.ajax({
			type: "POST", // HTTP method POST or GET
			url: "main.php?act=singup", //Where to make Ajax calls
			dataType:"JSON", // Data type, HTML, json etc.
			data: input, //Form variables
			success:function(response){

				$("#show_success_box").append(response.message);
				$("#show_success_box").css({
					"backgroundColor": "#00e600",
					"color":"#000000"
				});
				$("#singup_card").hide(); //show submit button
				$("#LoadingImage").hide(); //hide loading image
				textonError();



			},
			error:function (xhr, ajaxOptions, thrownError){
				$("#singup").show(); //show submit button
				$("#LoadingImage").hide(); //hide loading image
				alert(thrownError);
			}
			});
	});
// error
	function textonError(){
		if(sms.error > 0){
		$('#show_error_text').html(sms.messsage);
		$('#show_error_text').show('slow');

		$('#show_error_text').css({"backgroundColor":'#ff1a1a',
															"color":"#ffffff"
																});
	}else{
		$("#show_success_text").html(sms.messsage);
		$('#show_error_text').css({"backgroundColor":'#ff1a1a',
															"color":"#00e600"
																});

	}
	}

	$("#name").on("blur", function(e) {
      login();
	});
	var requesting = false;
	var timeout;
	$("#name").keypress(function(){
			if(requesting == false ){
				if(timeout) {
					clearTimeout(timeout);
					timeout = null;
				}

				timeout = setTimeout(login, 1000);
			}


	});


	function login(){
		var myname= "name="+$("#name").val();

		//var pass='password='+$('#password').val();
		jQuery.ajax({
			type:"POST",
			url: "main.php?act=login",
			dataType:"JSON",
			data: myname,
			success:function(result){
			//on success, hide  element user wants to delete.
			console.log("efe");
			console.log(result.error);
				console.log(result);
			}


		});

	}

	//##### Send delet/:e Ajax request to response.php #########
	$("body").on("click", "#responds .del_button", function(e) {
		 e.preventDefault();
		 var clickedID = this.id.split('-'); //Split ID string (Split works as PHP explode)
		 var DbNumberID = clickedID[1]; //and get number from array
		 var myData = 'recordToDelete='+ DbNumberID; //build a post data structure

		$('#item_'+DbNumberID).addClass( "sel" ); //change background of this element by adding class
		$(this).hide(); //hide currently clicked delete button

			jQuery.ajax({
			type: "POST", // HTTP method POST or GET
			url: "main.php?act=delete", //Where to make Ajax calls
			dataType:"text", // Data type, HTML, json etc.
			data:myData, //Form variables
			success:function(response){
				//on success, hide  element user wants to delete.
				console.log(response.error);
				$('#item_'+DbNumberID).fadeOut();
			},
			error:function (xhr, ajaxOptions, thrownError){
				//On error, we alert user
				alert(thrownError);
			}
			});
	});

});
