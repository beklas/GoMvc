<?php
require  'lib/Session.php';
Session::init();
require  'Core/Config.php';
require  'Core/Set.php';
require  'lib/Database.php';
require  'Core/Template.php';
require  'Core/Helpers.php';

require 'Templates'. DS . TEMPLATE . DS . 'header.php'; // start header

include 'Start.php';
$start = new Start();
$start->app(); //start applicacion

require 'Templates'. DS . TEMPLATE . DS . 'footer.php'; // start foooter
