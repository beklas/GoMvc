<?php
// start my library with Go static method
class Set
{
    public static $vars = array();

    public static function GoLanguage()
    {
        include 'Core/Lang.php';
        return    startLang();

    }
    public static function GoDb()
    {

        include 'Core/Database.php';
        return Db::conn();

    }

    public static function GoSet( $name, $value )
    {

        return    self::$vars[ $name ] = $value;

    }

}

$Lang = Set::GoLanguage(); // start method Go language and call languages to all files with startLang():
$Db   = Set::GoDb();       // start db conection and calal to another file with Db::conn()
