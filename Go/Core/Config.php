<?php
    define( "SITETITLE", "Go Mvc" );
    define( "SITE_LINK", "http://query.local" );
    define( "SITELANG", "en" );

    define( "SITE_EMAIL", "cipiklevis@gmail.com" );
    define( "ERRORVIEW", "View/error/" );
    define( "TEMPLATE", "Default" );
    define( "SERVERLOCAL", "localhost" );  //FOR TESTING IN LOCAL SERVER
    define( "SERVERONLINE", "localhost" ); // YOUR SERVER ONLNE
    define( "UPLOAD_DIR", "Uploading/Files" );



    $Helpers = array(
        '0' =>'Document',
        '1' =>'Database',
        '2' =>'Password',
        '3' =>'Mailer',
        '4' =>'Url',
        '5' =>'Session',
        '6' =>'Profiler',
        '7' =>'Date'
    );
