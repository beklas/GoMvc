<?php

class Db // for mysql PDO connection
{

    public static $username;
    public static $servername;
    public static $password;
    public static $dbname;
    private static $instance = NULL;

    private function __construct(){}
    private function __clone() {}

    public static function conn()

    {
        include 'Globals.php';

           self::$username   = $GLOBALS['user'];
           self::$servername = $GLOBALS['server'];
           self::$password   = $GLOBALS['pass'];
           self::$dbname     = $GLOBALS['dbname'];
           $dbtype           = $GLOBALS['dbtype'];

        if( !isset( self::$instance ) ):

            $pdo_Options[ PDO::ATTR_ERRMODE ]= PDO::ERRMODE_EXCEPTION;
            self::$instance= new PDO(
            'mysql:host=' .self::$servername.';dbname='.self::$dbname.';',self::$username,self::$password,$pdo_Options);
            /*
            self::$instance = new mysqli(self::$servername,self::$username,self::$password,self::$dbname);
            if( self::$instance->connect_error ):
                die("Error durante la connesione con il db" . self::$instance->connect_error());
            endif;
            */

        endif;

    return self::$instance;
    }
}
