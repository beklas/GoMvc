<?php

class Database
{
    public $end = false;

    /**
    *@param select query*
    * array data
    */
    public static function Select( $sql, $data_array=array() )
    {
        $sql =Db::conn()->prepare( $sql );

        foreach( $data_array as $key => $value ):

            if( is_int( $value ) ):
                $sql->bindValue( "$key", $value, PDO::PARAM_INT);
            else:
                $sql->bindValue("$key", $value);
            endif;

        endforeach;

        $sql->execute();

        return $sql->fetchAll();

        /*if($fetchMode === PDO::FETCH_CLASS):
            return $sql->fetchAll( $fetchMode, $class );
        else:
            return $sql->fetchAll( $fetchMode );
        endif;*/


    }

    public static function Insert( $table, $data )
    {
        ksort($data);

        $fieldColumn = implode(',', array_keys($data));
        $fieldValues = ':'.implode(', :', array_keys($data));

        $sql=Db::conn()->prepare(" INSERT INTO $table ( $fieldColumn ) VALUES ( $fieldValues ) ");

        foreach ($data as $key => $value):
                $sql->bindValue( ":$key", $value );

        endforeach;

        $sql->execute();

        return true;

    }
    public function Update( $table, $data, $where )
    {
        ksort( $data );

        $fieldDetails = null;

        foreach ( $data as $key => $value ):
            $fieldDetails .= "$key = :field_$key,";
        endforeach;

        $fieldDetails = rtrim($fieldDetails, ',');

        $whereDetails = null;

        $i = 0;

        foreach ( $where as $key => $value ):
            if ( $i == 0 ):
                $whereDetails .= "$key = :where_$key";
            else:
                $whereDetails .= " AND $key = :where_$key";
            endif;
            $i++;

        endforeach;

        $whereDetails = ltrim($whereDetails, ' AND ');

        $sql = Db::conn()->prepare("UPDATE $table SET $fieldDetails WHERE $whereDetails");

        foreach ( $data as $key => $value ):
            $sql->bindValue(":field_$key", $value);
        endforeach;

        foreach ( $where as $key => $value ):
            $sql->bindValue( ":where_$key", $value );
        endforeach;

        $sql->execute();

        return true;
    }

   public function Delete($table, $where, $limit = 1)
   {
       ksort($where);

       $whereDetails = null;

       $i = 0;

       foreach ($where as $key => $value ):
           if ( $i == 0 ):
               $whereDetails .= "$key = :$key";
           else:
               $whereDetails .= " AND $key = :$key";
           endif;
           $i++;

       endforeach;

       $whereDetails = ltrim($whereDetails, ' AND ');

       // If the limit is a number, use a limit on the query.
       if ( is_numeric( $limit ) ):
           $uselimit = "LIMIT $limit";
       endif;

       $sql = Db::conn()->prepare("DELETE FROM $table WHERE $whereDetails $uselimit");

       foreach ( $where as $key => $value ):
           $sql->bindValue( ":$key", $value );
       endforeach;

       $sql->execute();

       return true;
   }

}
