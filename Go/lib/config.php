<?php

echo "Db";

class Db
{
        public $username;
        public $servername;
        public $password;
        public $dbname;

        public function conn(){

         include 'globals.php';

            $this->username = $GLOBALS['user'];
            $this->servername = $GLOBALS['server'];
            $this->password = $GLOBALS['pass'];
            $this->dbname = $GLOBALS['dbname'];

            $mysql = new mysqli($this->servername, $this->username,$this->password,$this->dbname);
            if($mysql->connect_error){
                die("Error durante la connesione con il db" . $mysql->connect_error());
            }

            return $mysql;
        }

}
/*
$new = new Db();
$new->conn();
