<?php

class Start
{

    public function app()
    {

        $url = isset($_SERVER['PATH_INFO']) ? explode('/', ltrim($_SERVER['PATH_INFO'],'/')) : '/';

        if( $url == '/' ):
            // the home

            Template::home('pages','index');


        else:

            $requestController=$url[0];
            // first elemet is a controollerr;
            // seccond elemet dovra essere a method (action)
            $requestAction = isset ($url[1] )? $url[1] :'';
            // end of partas are the argument of the method;
            $requestParam = array_slice( $url, 2 );

            // check if the conttroller is exist
            // do that for model and view
            $shiftPath = __DIR__.'/src/Controller/'.$requestController.'Controller.php';

            if(file_exists( $shiftPath) ):

                require_once __DIR__.'/src/Model/'.$requestController.'Model.php';
                require_once __DIR__.'/src/Controller/'.$requestController.'Controller.php';
                //require_once __DIR__.'/View/'.$requestController.'View.php';

                $modelName      = ucfirst($requestController).'Model';
                $controllerName = ucfirst($requestController).'Controller';
                //$viewName       = ucfirst($requestController).'View';

                $controllerObj  = new $controllerName (new $modelName);
                //$viewObj        = new $viewName (new $controllerObj, new $modelName); // if you want a clas view example :: IndexView.php
                $viewpath = __DIR__.'/src/View/'.strtolower($requestController.'s').DS.strtolower($requestAction).'.php';

                if (file_exists($viewpath) && $requestAction != ''):
                             // then we call the method via the view
                             // dynamic call of the view
                            if($requestParam[0] != null):
                                $param0 = $requestParam[0];
                            else:
                                $param0 = '';
                            endif;

                            if($requestParam[1] != null):
                                $param1 = $requestParam[1];
                            else:
                                $param1 = '';
                            endif;

                            if($requestParam[2] != null):
                                $param2 = $requestParam[2];
                            else:
                                $param2 = '';
                            endif;

                            echo $controllerObj->$requestAction($param0,$param1,$param2);

                else:
                    Url::errorView('error.php');
                endif;

            else:

                Url::errorView('error.php');

            endif;

        endif;

    }
}
