<h2> <?php echo $data['title']; ?></h2>
<div class="panel-group">

<div class="panel panel-success">
  <div class="panel-heading">Users list</div>
  <div class="panel-body">
      <table class="table table-bordered">
            <thead>
              <tr>
                <th>Name</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ($data['user'] as $user) : ?>
                  <tr>
                    <td><?php echo $user['name']?></td>
                    <td>
                        <a href="/User/edit/<?php echo $user['id'];?>">Edit</a>,
                        <a href="/User/delete/<?php echo $user['id'];?>">Delete</a>,
                        <a href="/User/view/<?php echo $user['id'];?>">View</a>
                    </td>
                  </tr>
            <?php endforeach ?>
        </tbody>
      </table>


    </div>
</div>
</div>
