<?php

class AboutController
{
    private  $modelObj;


    public function __construct($model)
    {
            $this->modelObj = $model;
    }

    public function about()
    {

            $data['users'] =  $this->modelObj->users();
            Template::show('pages','about',$data,$title);

    }
    public function users()
    {

            $title = "Users";
            $data['users'] =  $this->modelObj->users();

            Template::show('users',$data, $title);

    }


}
