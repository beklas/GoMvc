<?php

class   UserController
{
    public $modelObj;

    public function __construct( $model )
    {
        $this->modelObj = $model;
    }

    public function login()
    {
            $data['title'] = "Login page";

        if( isset($_POST['submit']) ):
            if(Session::get('name')):
                $setflash = array( 'type'=>'flash_info','message'=>startLang()['users']['login_apaer'] );
            else:
                $name       = $_POST['name'];
                $password   = $_POST['password'];

                if( empty( $name ) && empty( $password ) ):

                        $setflash=array('type' =>'flash_error','message' => startLang()['users']['input_data'] );
                else:

                    $data = $this->modelObj->getUsername( $name, $password );

                    if( isset( $data ) ):

                            if( $name == $data['name'] ):
                                    Session::set( 'name',$data['name'] );
                                    Session::set( 'id',$data['id'] );
                                    //set seesion message for login (name_set);
                                    Session::GoFlash('login_set',startLang()['users']['login_success'],'flash_success');
                                    Url::redirectTo( '/User/profile' );

                            else:
                                    $setflash = array( 'type'=>'flash_error','message'=>startLang()['users']['error_name'] );
                            endif;

                    else:
                            $setflash = array( 'type'=>'flash_error','message'=>startLang()['users']['error_name'] );
                    endif;

                endif;

            endif;

        endif;


        if(Session::GoFlash('logout_set')):
            Session::GoFlash('logout_set');
        endif;

        Template::show( 'users','login',$data,$setflash );

    }

    public function home()
    {
        $date['title'] = 'Home';
        $data['users'] = $this->modelObj->getUsers();

        Template::show( 'users','home',$data,$setflash );
    }

    public function user()
    {
        $data['title'] = 'Users';
        echo Date::difference('12-12-2012','21-04-2016');
        if(Session::GoFlash('delete_set')):
            Session::GoFlash('delete_set');
        endif;

        $data['user']  = $this->modelObj->getUsers();

        Template::show('users','user', $data, $setflash);
    }

    public function view($iduser)
    {
        $data['title'] = 'Users view';

        $data['user']  = $this->modelObj->viewUser($iduser);

        Template::show('users','view', $data, $setflash);
    }

    public function delete($iduser)
    {
        $data['title'] = 'Users view';
        $where = array(
            'id'=>$iduser
        );
        $data  = $this->modelObj->deleteUser($where);

        if( isset( $data ) ):
            Session::GoFlash('delete_set',startLang()['users']['delete_success'],'flash_success');
            Url::redirectTo('/User/user');
        endif;
    }

    public function edit( $iduser )
    {
        $data['title'] = 'Edit Users';
        if( empty( $iduser ) ):
            $iduser = Session::get('id');
        endif;

        if( !empty( $iduser ) ):

            if( $_POST['updateUser'] ):
                $name = $_POST['name'];

                if( !empty($name ) ):

                    $gopostdata = array(
                        'name' => $name
                    );
                    $where = array(
                        'id' =>  $iduser
                    );
                    $data = $this->modelObj->updateUser( $gopostdata, $where);
                    if( isset( $data ) ):
                        $setflash=array('type' =>'flash_success','message' => startLang()['users']['update_success'] );
                    endif;

                else:

                    $setflash=array('type' =>'flash_error','message' => startLang()['users']['input_data'] );
                endif;

            endif;
        else:
            $setflash=array('type' =>'flash_error','message' => startLang()['users']['update_error'] );
        endif;

        $data['user']  = $this->modelObj->getUser($iduser);

        Template::show('users','edit', $data, $setflash);
    }

    public function profile()
    {
        if( Session::GoFlash('login_set') ):
            Session::GoFlash('login_set');
        endif;

        Template::show( 'users','profile', $data, $setflash );
    }

    public function records()
    {
        $data['title']      = "Recods page";
        $iduser = Session::get('id');
        $content = $_POST['content'];


        if( $_POST['insertRecords'] ):
            if( empty( $content ) || empty( $iduser ) ):
                $setflash = array('type'=>'flash_error','message'=>startLang()['records']['insert_error']);

            else:

                $gopostdata = array(
                    'content'=> $content,
                    'users_id'=> $iduser
                );

                $data = $this->modelObj->insertContent($gopostdata);

                if( $data['message'] == 0 ):
                    $setflash = array('type'=>'flash_success','message'=>startLang()['records']['insertrecord']);
                endif;

            endif;

        endif;



        $data['records']    = $this->modelObj->getRecords( $iduser );

        Template::show( 'users', 'records', $data, $setflash );

    }


    public function logout()
    {

        Session::destroy();
        Session::GoFlash('logout_set',startLang()['users']['login_success'],'flash_info');
        Url::redirectTo( '/User/login' );


    }

}
