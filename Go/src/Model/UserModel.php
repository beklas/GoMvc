<?php

/**
 *
 */
class  UserModel
{
    public $title;
    //public $message = array();

    public function __construct()
    {
        $this->title;
        //$this->message = $message;
    }

    public function getUsername( $name, $password )
    {

        $db  = Db::conn();
        $sql = $db->query( 'SELECT * FROM users where name = "'.$name.'" and password = "'.$password.'" LIMIT 1' );
        $record = $sql->fetch();
        return   $record;

    }
    public function getRecords( $iduser )
    {
        return Database::Select("SELECT
                r.content,
                r.users_id,
                u.id,
                u.name
                FROM records AS r INNER JOIN users AS u
                ON (r.users_id = u.id) WHERE r.users_id = :id",
                array(':id'=>$iduser)
            );
    }

    public function getUsers()
    {
        return  Database::Select("SELECT * FROM users ");

    }

    public function getUser($iduser)
    {
        return  Database::Select("SELECT * FROM users WHERE id = :id",array(':id'=>$iduser));
    }

    public function viewUser($iduser)
    {
        return  Database::Select("SELECT * FROM users WHERE id = :id",array(':id'=>$iduser));

    }
    public function deleteUser($where)
    {
        return Database::Delete( 'users', $where );

    }
    public function insertContent( $gopostdata )
    {
         $sql = Database::Insert( 'records', $gopostdata );

         if( $sql ):
             $message['message'] = 0;
         else:
             $message['message'] = 1;
         endif;

         return $message;
    }
    public function updateUser( $gopostdata, $where)
    {
        return Database::Update( 'users', $gopostdata, $where );

    }

}
