<?php

$lang=array(
    "success"       =>"OK registered successfully",
    "success_error" =>"Erro non registered",
    "file_exists"   =>"Il file non esiste",
    "panel_user"    =>"Panel for users",
    "titlerecord"   =>"Records",
    "titlehome"     =>"Home",
    "titleprofile"  =>"Profile",
        // users
        "users" => array(
            'title_login'       => "Login page!",
            'name_login'        => "Your Name",
            'password_login'    =>'Your Password',
            'input_data'        =>'Please insert your data',
            'error_name'        =>'Your username dosent exist in database!',
            'error_login'       =>'Error generiting ... Please try again and put the correct data!',
            'error_select_name' =>'Nothing to show please try again!',
            'login_success'     =>'You are logged successfully!',
            'login_apaer'       =>'You are logged!',
            'update_error'      =>'This user dosent exist or data are fake, Plese try again and check your data!',
            'update_success'    =>'Data successfully Updatet',
            'delete_success'    =>'Delete successfully',


        ),
        "records" => array(
            'insertrecord'      =>"All date insertet successfully",
            'insert_error'      =>"Error generiting, try again"

        ),

);
